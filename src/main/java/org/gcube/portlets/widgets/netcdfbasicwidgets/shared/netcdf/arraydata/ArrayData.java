package org.gcube.portlets.widgets.netcdfbasicwidgets.shared.netcdf.arraydata;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi 
 *
 *
 */
public class ArrayData implements Serializable {

	private static final long serialVersionUID = -5819190300039031091L;

	public ArrayData(){
		super();
		
	}
	
	public String asString(){
		return "";
	}

	@Override
	public String toString() {
		return "ArrayData []";
	}
	
	
	
	
}
