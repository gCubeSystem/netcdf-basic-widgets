This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "netcdf-basic-widgets"



## [v1.2.0] - 2021-10-06

### Fixed

- Updated dependencies



## [v1.1.0] - 2018-10-01

### Features

- Removed HomeLibrary dependency



## [v1.0.0] - 2017-11-09

### Features

- First Release


