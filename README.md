# NetCDF Basic Widgets

NetCDF Basic Widgets is a set of widgets that allow the display of information related to NetCDF files.

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* Use of this widgets is is described on [Wiki](https://wiki.gcube-system.org/gcube/DataMiner_Manager).

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/netcdf-basic-widgets/releases).

## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)